import React, { useEffect } from 'react';
import './App.css';

const App: React.FC = () => {
  
  const [checked, setChecked] = React.useState(false);

  const isOn = (url: string) : boolean => {
    const setting = localStorage.getItem(url);
    if (setting === "true") {
      return true;
    } else {
      return false;
    }
  }

  const swapOn = (url: string) => {
    if (isOn(url)) {
      localStorage.setItem(url, "false");
    } else {
      localStorage.setItem(url, "true");
    }
  }

  const handleEdit = async () => {
    swapOn(getUrl());
    setCheckBox();

    let mode = isOn(getUrl()) ? "on" : "off";

    await browser.tabs.executeScript(null, {
      code: `document.designMode = "${mode}"`
    });
  }

  const getUrl = () : string => {
    const tab = browser.tabs.getCurrent();
    return tab.url;
  }

  const setCheckBox = () => {
    setChecked(isOn(getUrl()));
  }

  useEffect(() => {
    setCheckBox();
  });

  return (
    <div className="App">
      <h1>Edit Every Page</h1>
      <br />
      <input type="checkbox" checked={checked} onChange={(e) => {handleEdit().then()}} />
    </div>
  );
}

export default App;
